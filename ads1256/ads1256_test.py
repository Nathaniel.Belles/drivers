import ads1256_spidev as inputs
import time

ads0 = inputs.ADS1256(0, 0)
ads1 = inputs.ADS1256(0, 1)

print(ads0.ReadID())
print(ads1.ReadID())


def GetInputs(adc0, adc1):
    inputs = {}
    adc0.SetADCChannel(adc0.MUX_AIN0, adc0.MUX_AINCOM)
    adc1.SetADCChannel(adc1.MUX_AIN0, adc1.MUX_AINCOM)
    inputs["R8"] = adc0.ReadADCChannel()
    inputs["R0"] = adc1.ReadADCChannel()

    adc0.SetADCChannel(adc0.MUX_AIN1, adc0.MUX_AINCOM)
    adc1.SetADCChannel(adc1.MUX_AIN1, adc1.MUX_AINCOM)
    inputs["R9"] = adc0.ReadADCChannel()
    inputs["R1"] = adc1.ReadADCChannel()

    adc0.SetADCChannel(adc0.MUX_AIN2, adc0.MUX_AINCOM)
    adc1.SetADCChannel(adc1.MUX_AIN2, adc1.MUX_AINCOM)
    inputs["V0"] = adc0.ReadADCChannel()
    inputs["R2"] = adc1.ReadADCChannel()

    adc0.SetADCChannel(adc0.MUX_AIN3, adc0.MUX_AINCOM)
    adc1.SetADCChannel(adc1.MUX_AIN3, adc1.MUX_AINCOM)
    inputs["V1"] = adc0.ReadADCChannel()
    inputs["R3"] = adc1.ReadADCChannel()

    adc0.SetADCChannel(adc0.MUX_AIN4, adc0.MUX_AINCOM)
    adc1.SetADCChannel(adc1.MUX_AIN4, adc1.MUX_AINCOM)
    inputs["I0"] = adc0.ReadADCChannel()
    inputs["R4"] = adc1.ReadADCChannel()

    adc0.SetADCChannel(adc0.MUX_AIN5, adc0.MUX_AINCOM)
    adc1.SetADCChannel(adc1.MUX_AIN5, adc1.MUX_AINCOM)
    inputs["I1"] = adc0.ReadADCChannel()
    inputs["R5"] = adc1.ReadADCChannel()

    adc1.SetADCChannel(adc1.MUX_AIN6, adc1.MUX_AINCOM)
    inputs["R6"] = adc1.ReadADCChannel()

    adc1.SetADCChannel(adc1.MUX_AIN7, adc1.MUX_AINCOM)
    inputs["R7"] = adc1.ReadADCChannel()

    digital0 = adc0.ReadGPIO()
    digital1 = adc1.ReadGPIO()
    inputs["D0"] = digital0[1]
    inputs["D1"] = digital0[0]
    inputs["D2"] = digital1[3]
    inputs["D3"] = digital1[2]
    inputs["D4"] = digital1[1]
    inputs["D5"] = digital1[0]

    return inputs


while True:
    time.sleep(1)
    print(GetInputs(ads0, ads1))
    print("")
