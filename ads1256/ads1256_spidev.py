import time
import wiringpi as wp
import spidev
import random


def debug_print(string, spacing=0, backend=False):
    PRINT_LEVEL = 0
    PRINT_BACKEND = False

    result = ""
    for i in range(spacing):
        result += "\t"
    result += string

    if backend == False:
        if PRINT_LEVEL >= spacing:
            print(result)
    else:
        if PRINT_BACKEND == True:
            print(result)


class ADS1256:
    """
    A class to interface with the TI ADS1256 ADC chip.

    How to use:
        Set any configuration variables in the __setup_variables_config()
        function using the pre-set variables in the __setup_variables_ads1256()
        function where applicable.

        If the function you are trying to use doesn't have a description or
        documentation section, you probably shouldn't be using it. If attempting
        to use this library in personal project, verify the pinout and
        configuration variables for your situation in the
        __setup_variables_ads1256() and __setup_variables_config() functions.

    Attributes
    ----------
        bus : integer
            SPI bus to use on Raspberry Pi (normally 0)
        device: integer
            SPI device to use on Raspbbery Pi (normally 0 or 1)

    Configuration
    -------------
        CONF_00 : byte
            STATUS register: MSB/LSB first, autocalibration, buffer
        CONF_01 : byte
            MUX register: AIN0 to AINCOM
        CONF_02 : byte
            ADCON register: clock, sensor detect, gain
        CONF_03 : byte
            DRATE register: update frequency in samples per second
        CONF_04 : byte
            IO register: enable inputs/outputs and state of pins

    Methods
    -------
        ADS1256(bus, device):
            Instantiates an object of type ADS1256
        ReadID():
            Gets the ID of the ADS1256 (usually 3)
        SetADCChannel(pos, neg):
            Sets the channels on the ADC input mux and starts sampling
        ReadADCChannel():
            Gets the voltage on the previously set pin
        GetGPIO()
            Gets the state of the digital inputs on the GPIO pins

    """

    def __init__(self, bus, device):
        # Debug print
        debug_print("Entering __init__()", 1, True)

        # Store arguments
        self.bus = bus
        self.device = device
        self.random = random.uniform(0, 1)

        # Run setup functions
        self.__setup_variables_ads1256()
        self.__setup_variables_config()
        self.__setup_config_pinout()
        self.__setup_config_spi()
        self.__setup_config_ads1256()

        # Debug print
        debug_print("Exiting __init__()", 1, True)

    def __del__(self):
        # Close spi interface
        self.spi.close()

    def __setup_variables_ads1256(self):
        # Debug print
        debug_print("Entering __setup_variables_ads1256()", 2, True)

        # ADS1256-specific SPI variables
        self.SPI_MODE = 0b01  # Clock Polarity 0, Clock Phase 1
        self.SPI_FREQUENCY = 1000000  # The ADS1256 supports 768kHz to 1.92MHz
        self.DRDY_TIMEOUT = 0.5  # Seconds to wait for DRDY when communicating
        self.DATA_TIMEOUT = 0.00005  # 50uS delay for sending data
        self.SCLK_FREQUENCY = 7680000  # Default clock rate is 7.68MHz
        self.VREF = 2.5  # Voltage reference = 2.5V

        # Register addresses
        self.REG_STATUS = 0x00
        self.REG_MUX = 0x01
        self.REG_ADCON = 0x02
        self.REG_DRATE = 0x03
        self.REG_IO = 0x04
        self.REG_OFC0 = 0x05
        self.REG_OFC1 = 0x06
        self.REG_OFC2 = 0x07
        self.REG_FSC0 = 0x08
        self.REG_FSC1 = 0x09
        self.REG_FSC2 = 0x0A
        self.NUM_REG = 11

        # Sample rates
        self.DRATE_30000 = 0xF0  # 30,000SPS (default)
        self.DRATE_15000 = 0xE0  # 15,000SPS
        self.DRATE_7500 = 0xD0  # 7,500SPS
        self.DRATE_3750 = 0xC0  # 3,750SPS
        self.DRATE_2000 = 0xB0  # 2,000SPS
        self.DRATE_1000 = 0xA0  # 1,000SPS
        self.DRATE_500 = 0x92  # 500SPS
        self.DRATE_100 = 0x82  # 100SPS
        self.DRATE_60 = 0x72  # 60SPS
        self.DRATE_50 = 0x63  # 50SPS
        self.DRATE_30 = 0x53  # 30SPS
        self.DRATE_25 = 0x43  # 25SPS
        self.DRATE_15 = 0x33  # 15SPS
        self.DRATE_10 = 0x23  # 10SPS
        self.DRATE_5 = 0x13  # 5SPS
        self.DRATE_2_5 = 0x03  # 2.5SPS

        # Commands
        self.CMD_WAKEUP = 0x00  # Completes SYNC and exits standby mode
        self.CMD_RDATA = 0x01  # Read data
        self.CMD_RDATAC = 0x03  # Start read data continuously
        self.CMD_SDATAC = 0x0F  # Stop read data continuously
        self.CMD_RREG = 0x10  # Read from register
        self.CMD_WREG = 0x50  # Write to register
        self.CMD_SELFCAL = 0xF0  # Offset and gain self-calibration
        self.CMD_SELFOCAL = 0xF1  # Offset self-calibration
        self.CMD_SELFGCAL = 0xF2  # Gain self-calibration
        self.CMD_SYSOCAL = 0xF3  # System offset calibration
        self.CMD_SYSGCAL = 0xF4  # System gain calibration
        self.CMD_SYNC = 0xFC  # Synchronize the A/D conversion
        self.CMD_STANDBY = 0xFD  # Begin standby mode
        self.CMD_RESET = 0xFE  # Reset to power-on values

        # Status enable bits
        self.STATUS_BUFFER_ENABLE = 0x02
        self.STATUS_AUTOCAL_ENABLE = 0x04
        self.STATUS_ORDER_LSB = 0x08

        # Gain levels
        self.AD_GAIN_1 = 0x00
        self.AD_GAIN_2 = 0x01
        self.AD_GAIN_4 = 0x02
        self.AD_GAIN_8 = 0x03
        self.AD_GAIN_16 = 0x04
        self.AD_GAIN_32 = 0x05
        self.AD_GAIN_64 = 0x06

        # Sensor Detect Current Sources
        self.AD_SDCS_OFF = 0x00
        self.AD_SDCS_500pA = 0x08
        self.AD_SDCS_2uA = 0x10
        self.AD_SDCS_10uA = 0x18

        # Clock divider
        self.AD_CLK_OFF = 0x00
        self.AD_CLK_EQUAL = 0x20
        self.AD_CLK_HALF = 0x40
        self.AD_CLK_FOURTH = 0x60

        # Mux channel selection
        self.MUX_AIN0 = 0x0
        self.MUX_AIN1 = 0x1
        self.MUX_AIN2 = 0x2
        self.MUX_AIN3 = 0x3
        self.MUX_AIN4 = 0x4
        self.MUX_AIN5 = 0x5
        self.MUX_AIN6 = 0x6
        self.MUX_AIN7 = 0x7
        self.MUX_AINCOM = 0x8

        # I/O
        self.IO_INPUTS = 0xFF
        self.IO_OUTPUTS_LOW = 0x00
        self.IO_OUTPUTS_HIGH = 0x0F

        # Calibration
        self.CALIBRATION = {
            self.DRATE_30000: {"alpha": 0x400000, "beta": 1.8639},
            self.DRATE_15000: {"alpha": 0x400000, "beta": 1.8639},
            self.DRATE_7500: {"alpha": 0x400000, "beta": 1.8639},
            self.DRATE_3750: {"alpha": 0x400000, "beta": 1.8639},
            self.DRATE_2000: {"alpha": 0x3C0000, "beta": 1.7474},
            self.DRATE_1000: {"alpha": 0x3C0000, "beta": 1.7474},
            self.DRATE_500: {"alpha": 0x3C0000, "beta": 1.7474},
            self.DRATE_100: {"alpha": 0x4B0000, "beta": 2.1843},
            self.DRATE_60: {"alpha": 0x3E8000, "beta": 1.8202},
            self.DRATE_50: {"alpha": 0x4B0000, "beta": 2.1843},
            self.DRATE_30: {"alpha": 0x3E8000, "beta": 1.8202},
            self.DRATE_25: {"alpha": 0x4B0000, "beta": 2.1843},
            self.DRATE_15: {"alpha": 0x3E8000, "beta": 1.8202},
            self.DRATE_10: {"alpha": 0x5DC000, "beta": 2.7304},
            self.DRATE_5: {"alpha": 0x5DC000, "beta": 2.7304},
            self.DRATE_2_5: {"alpha": 0x5DC000, "beta": 2.7304},
        }

        # Debug print
        debug_print("Exiting __setup_variables_ads1256()", 2, True)

    def __setup_variables_config(self):
        # Debug print
        debug_print("Entering __setup_variables_config()", 2, True)

        # Check what pins to use
        if self.bus == 0 and self.device == 0:
            # Physical GPIO pin numbering
            self.CS_PIN = 24
            self.DRDY_PIN = 11
            self.RESET_PIN = 15
            self.PDWN_PIN = 13
        elif self.bus == 0 and self.device == 1:
            # Physical GPIO pin numbering
            self.CS_PIN = 26
            self.DRDY_PIN = 22
            self.RESET_PIN = 16
            self.PDWN_PIN = 18
        else:
            debug_print("Invalid configuration!")

        # STATUS: Set MSB first, enable autocalibration, disable buffer
        self.CONF_00 = self.STATUS_AUTOCAL_ENABLE  # 0x04

        # MUX: AIN0 to AINCOM
        self.CONF_01 = (self.MUX_AIN0 << 4) | (self.MUX_AINCOM << 0)  # 0x08

        # ADCON: Disable clock, disable sensor detect, gain = 1
        self.CONF_02 = self.AD_CLK_OFF | self.AD_SDCS_OFF | self.AD_GAIN_1  # 0x00

        # DRATE: 30,000 samples per second
        self.CONF_03 = self.DRATE_50  # 0x63

        # IO: Set all to inputs
        self.CONF_04 = self.IO_INPUTS  # 0xFF

        # Debug print
        debug_print("Exiting __setup_variables_config()", 2, True)

    def __setup_config_pinout(self):
        # Debug print
        debug_print("Entering __setup_config_pinout()", 2, True)

        # Set up the wiringpi object to use physical pin numbers
        wp.wiringPiSetupPhys()

        # Initialize the DRDY pin
        wp.pinMode(self.DRDY_PIN, wp.INPUT)

        # Initialize the RESET pin
        wp.pinMode(self.RESET_PIN, wp.OUTPUT)
        wp.digitalWrite(self.RESET_PIN, wp.HIGH)
        time.sleep(self.random)
        wp.digitalWrite(self.RESET_PIN, wp.LOW)
        time.sleep(self.random)
        wp.digitalWrite(self.RESET_PIN, wp.HIGH)

        # Initialize PDWN pin
        wp.pinMode(self.PDWN_PIN, wp.OUTPUT)
        wp.digitalWrite(self.PDWN_PIN, wp.HIGH)
        time.sleep(self.random)
        wp.digitalWrite(self.RESET_PIN, wp.LOW)
        time.sleep(self.random)
        wp.digitalWrite(self.RESET_PIN, wp.HIGH)

        # Initialize CS pin
        wp.pinMode(self.CS_PIN, wp.OUTPUT)
        wp.digitalWrite(self.CS_PIN, wp.HIGH)

        # Debug print
        debug_print("Exiting __setup_config_pinout()", 2, True)

    def __setup_config_spi(self):
        # Debug print
        debug_print("Entering __setup_config_spi()", 2, True)

        # Setup SPI interface
        self.spi = spidev.SpiDev()
        self.spi.open(self.bus, self.device)
        self.spi.max_speed_hz = self.SPI_FREQUENCY
        self.spi.no_cs = True
        self.spi.mode = self.SPI_MODE

        # Debug print
        debug_print("Exiting __setup_config_spi()", 2, True)

    def __setup_config_ads1256(self):
        # Debug print
        debug_print("Entering __setup_config_ads1256()", 2, True)

        # Send configuration as stored in self.__setup_variables_config()
        self.WriteReg(self.REG_STATUS, self.CONF_00)  # 0x50 00 04
        self.WriteReg(self.REG_MUX, self.CONF_01)  # 0x51 00 08
        self.WriteReg(self.REG_ADCON, self.CONF_02)  # 0x52 00 00
        self.WriteReg(self.REG_DRATE, self.CONF_03)  # 0x53 00 XX
        self.WriteReg(self.REG_IO, self.CONF_04)  # 0x54 00 FF

        # Send startup commands
        self.SendCommand(self.CMD_SDATAC)  # 0x0F
        self.SendCommand(self.CMD_SELFCAL)  # 0xF0

        # Wait for self calibration to complete
        self.WaitDRDY()

        # Read calibration data
        self.OFC0 = self.ReadReg(self.REG_OFC0)
        self.OFC1 = self.ReadReg(self.REG_OFC1)
        self.OFC2 = self.ReadReg(self.REG_OFC2)
        self.FSC0 = self.ReadReg(self.REG_FSC0)
        self.FSC1 = self.ReadReg(self.REG_FSC1)
        self.FSC2 = self.ReadReg(self.REG_FSC2)

        # Concatenate calibration data
        self.FSC = self.FSC2 << 16 | self.FSC1 << 8 | self.FSC0 << 0
        self.OFC = self.OFC2 << 16 | self.OFC1 << 8 | self.OFC0 << 0

        # Look up what calibration variables to use
        self.ALPHA = self.CALIBRATION[self.CONF_03]["alpha"]
        self.BETA = self.CALIBRATION[self.CONF_03]["beta"]

        # Wait for data to be ready before allowing data to be read
        self.WaitDRDY()

        # Debug print
        debug_print("Exiting __setup_config_ads1256()", 2, True)

    def __ChipSelect(self):
        wp.digitalWrite(self.CS_PIN, wp.LOW)
        debug_print("ChipSelect", 3)

    def __ChipRelease(self):
        wp.digitalWrite(self.CS_PIN, wp.HIGH)
        debug_print("ChipRelease", 3)

    def __SendByte(self, mybyte):
        # Debug print
        debug_print("Entering __SendByte()", 3)

        # Convert input to character
        mydata = chr(mybyte)

        # Debug print
        debug_print("Data to send: " + str(mybyte) + " hex: " + hex(mybyte), 4)

        # Write byte to SPI bus
        self.spi.writebytes([mybyte])

        # Debug print
        debug_print("Exiting __SendByte()", 3)

    def __ReadByte(self):
        # Debug print
        debug_print("Entering __ReadByte()", 3)

        # Read byte from SPI bus
        MISObyte = self.spi.readbytes(1)

        # Debug print
        debug_print(
            "Data to send: " + str(MISObyte[0]) + " hex: " + hex(MISObyte[0]), 4
        )

        # Debug print
        debug_print("Exiting __ReadByte()", 3)

        return MISObyte[0]

    def __DataDelay(self):
        # Debug print
        debug_print("Entering __DataDelay()", 3)

        start = time.time()
        elapsed = time.time() - start

        # Wait for TIMEOUT to elapse
        while elapsed < self.DATA_TIMEOUT:
            elapsed = time.time() - start

        # Debug print
        debug_print("Exiting __DataDelay", 3)

    def __TwosComplement(self, value, length):
        # Check if value needs to be modified
        if (value & (1 << (length - 1))) != 0:
            value = value - (1 << length)

        # Return value
        return value

    def __DelayMicroseconds(self, delayus):
        wp.delayMicroseconds(delayus)

    def WaitDRDY(self):
        """
        Waits for DRDY pin to be ready

        This is used to make sure that the data has been sampled properly and is used
        when calling the ReadID() and ReadADCChannel() functions. It verifies that
        when reading data that it is the most up to date data.

            Parameters:
                None.

            Returns:
                None.
        """

        # Debug print
        debug_print("Entering WaitDRDY()", 2)

        # Calculate elapsed timeout based on current time
        start = time.time()
        elapsed = time.time() - start

        # Store value of the DRDY pin
        drdy_level = wp.digitalRead(self.DRDY_PIN)

        # Wait for DRDY pin to go low or timeout to pass
        while (drdy_level == wp.HIGH) and (elapsed < self.DRDY_TIMEOUT):
            # Recalculate the elapsed time
            elapsed = time.time() - start

            # Get value of DRDY pin again
            drdy_level = wp.digitalRead(self.DRDY_PIN)

        # Check if timeout has been exceeded
        if elapsed >= self.DRDY_TIMEOUT:
            debug_print("WaitDRDY() timed out!")

        # Debug print
        debug_print("Exiting WaitDRDY()", 2)

    def ReadReg(self, register):
        # Debug print
        debug_print("Entering ReadReg(" + str(register) + ")", 2)

        # Select the chip
        self.__ChipSelect()

        # Start transaction
        self.__SendByte(self.CMD_RREG | register)  # Read from register
        self.__SendByte(0x00)  # Number of additional bytes
        self.__DataDelay()  # Wait for data to be ready
        read = self.__ReadByte()  # Read the data

        # Release the chip
        self.__ChipRelease()

        # Debug print
        debug_print("Exiting ReadReg()", 2)

        return read

    def WriteReg(self, register, data):
        # Debug print
        debug_print("Entering WriteReg(" + str(register) + ", " + str(data) + ")", 2)

        # Select the chip
        self.__ChipSelect()

        # Start transaction
        self.__SendByte(self.CMD_WREG | register)  # Write to register
        self.__SendByte(0x00)  # Number of additional bytes
        self.__SendByte(data)  # Send the data

        # Release the chip
        self.__ChipRelease()

        # Debug print
        debug_print("Exiting WriteReg()", 2)

    def SendCommand(self, command):
        # Debug print
        debug_print("Entering SendCommand(" + str(command) + ")", 2)

        # Select the chip
        self.__ChipSelect()

        # Write byte
        self.__SendByte(command)

        # Release the chip
        self.__ChipRelease()

        # Debug print
        debug_print("Exiting SendCommand()", 2)

    def ReadID(self):
        """
        Gets the ID of the ADS1256 (usually 3)

        This can be used to verify proper communcation with the chip. If the value that
        comes back is not 3, there is likely an issue with communciation and the data
        should be verified with a logic analyzer.

            Parameters:
                None.

            Returns:
                result (int): ID of the connected ADS1256 chip
        """
        # Debug print
        debug_print("Entered ReadID()", 1)

        # Wait for data to be ready
        self.WaitDRDY()

        # Get the value of the STATUS register
        value = self.ReadReg(self.REG_STATUS)

        # Convert to ID
        id = value >> 4

        # Print out raw byte
        debug_print("STATUS byte: " + str(value), 2)

        # Debug print
        debug_print("Exiting ReadID()", 1)

        # Return
        return id

    def SetADCChannel(self, pos, neg):
        """
        Gets the voltage on the previously set pin

        Call the SetADCChannel() function before using this function to set the pins
        desired. Once the SetADCChannel() function has been called, call this function
        immediately to get the voltage on that pin.

        Use the following possible values for the positive and negative inputs (as seen
        in the __setup_variables_ads1256() function):

            # Mux channel selection
            self.MUX_AIN0 = 0x0
            self.MUX_AIN1 = 0x1
            self.MUX_AIN2 = 0x2
            self.MUX_AIN3 = 0x3
            self.MUX_AIN4 = 0x4
            self.MUX_AIN5 = 0x5
            self.MUX_AIN6 = 0x6
            self.MUX_AIN7 = 0x7
            self.MUX_AINCOM = 0x8

        Note: DRDY pin will be left high at the end of this function while ADC reads the
        value of the current selected channel. ReadADCChannel() will wait for DRDY to go
        low again before reading the bytes associated with that channel.

            Parameters:
                pos (int): pin for the positive side of the ADC to select
                neg (int): pin for the negative side of the ADC to select

            Returns:
                None.
        """

        # Debug print
        debug_print("Entering SetInputMux(" + str(pos) + ", " + str(neg) + ")", 1)

        # Check that the AINCOM pin is not higher than any other pins
        if pos == self.MUX_AINCOM and neg != self.MUX_AINCOM:
            # Debug print
            debug_print("Invalid mux selection!")

            # Adjust pins to make sure valid pin selection
            pos = self.MUX_AIN0
            neg = self.MUX_AINCOM

        # Select the chip
        self.__ChipSelect()

        # Start transaction
        self.__SendByte(self.CMD_WREG | self.REG_MUX)
        self.__SendByte(0x00)
        self.__SendByte((pos << 4) | (neg << 0))

        # Release the chip
        self.__ChipRelease()

        # Syncronize the ADC
        self.__ChipSelect()
        self.__SendByte(self.CMD_SYNC)
        self.__ChipRelease()
        self.__DelayMicroseconds(10)

        # Wakeup the ADC
        self.__ChipSelect()
        self.__SendByte(self.CMD_WAKEUP)
        self.__ChipRelease()
        self.__DelayMicroseconds(10)

        # Debug print
        debug_print("Exiting SetInputMux()", 1)

    def ReadADCChannel(self):
        """
        Gets the voltage on the previously set pin

        Call the SetADCChannel() function before using this function to set the pins
        desired. Once the SetADCChannel() function has been called, call this function
        immediately to get the voltage on that pin.

            Parameters:
                None.

            Returns:
                result (float): voltage on the previously set pin
        """

        # Debug print
        debug_print("Entering ReadADC()", 1)

        # Select the chip
        self.__ChipSelect()

        # Wait for data sampling to finish
        self.WaitDRDY()

        # Send the read command
        self.__SendByte(self.CMD_RDATA)

        # Wait for data to be ready for reading
        self.__DataDelay()

        # Read the individual bytes from ADC
        byte0 = self.__ReadByte()
        byte1 = self.__ReadByte()
        byte2 = self.__ReadByte()

        # Release the SPI bus
        self.__ChipRelease()

        # Debug print
        debug_print("Bytes: " + hex(byte0) + " " + hex(byte1) + " " + hex(byte2), 3)

        # Concatenate the bytes
        result_complement = (byte0 << 16) + (byte1 << 8) + byte2
        debug_print("Concatednated: " + str(result_complement), 3)

        # Remove two's complement
        result_uncalibrated = self.__TwosComplement(result_complement, 24)
        debug_print("Uncalibrated: " + str(result_uncalibrated), 3)

        # Calibrate the result
        result = (
            2
            * self.VREF
            / (2 ** self.AD_GAIN_1)
            * (
                (result_uncalibrated / self.BETA / self.FSC)
                + (self.__TwosComplement(self.OFC, 24) / self.ALPHA)
            )
        ) + 5.0

        # Debug print
        debug_print("Calibrated output: " + str(result), 3)

        # Debug print
        debug_print("Exiting SetInputMux()", 1)

        # Return result
        return result

    def ReadGPIO(self):
        """
        Gets the digital inputs from the ADS1256's GPIO pins.

        The state of the pin is stored as an integer where 0 means low and 1 means
        high. The values returned are returned in the order of increasing index value
        for ease of accessing by index.

            Parameters:
                None.

            Returns:
                (b0, b1, b2, b3) (int): four-position integer tuple of the pin states
        """

        # Debug print
        debug_print("Entering ReadGPIO()", 1)

        # Read the byte containing digital inputs
        byte = self.ReadReg(self.REG_IO)

        # Parse the byte
        b0 = (0x01 & byte) >> 0
        b1 = (0x02 & byte) >> 1
        b2 = (0x04 & byte) >> 2
        b3 = (0x08 & byte) >> 3

        # Print the bits
        debug_print(
            "Bits: b0: "
            + str(b0)
            + ", b1: "
            + str(b1)
            + ", b2: "
            + str(b2)
            + ", b3: "
            + str(b3),
            3,
        )

        # Debug print
        debug_print("Exiting ReadGPIO()", 1)

        # Return result
        return (b0, b1, b2, b3)
