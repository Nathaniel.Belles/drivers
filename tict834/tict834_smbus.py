from smbus2 import SMBus, i2c_msg
import time


class TicI2C(object):
    def __init__(self, bus, address):
        self.bus = bus
        self.address = address

    # Sends the "Exit safe start" command.
    def exit_safe_start(self):
        command = [0x83]
        write = i2c_msg.write(self.address, command)
        self.bus.i2c_rdwr(write)

    # Sets the target position.
    def set_target_position(self, target):
        command = [
            0xE0,
            target >> 0 & 0xFF,
            target >> 8 & 0xFF,
            target >> 16 & 0xFF,
            target >> 24 & 0xFF,
        ]
        write = i2c_msg.write(self.address, command)
        self.bus.i2c_rdwr(write)

    # Gets one or more variables from the Tic.
    def get_variables(self, offset, length):
        write = i2c_msg.write(self.address, [0xA1, offset])
        read = i2c_msg.read(self.address, length)
        self.bus.i2c_rdwr(write, read)
        return list(read)

    # Gets the "Current position" variable from the Tic.
    def get_current_position(self):
        b = self.get_variables(0x22, 4)
        position = b[0] + (b[1] << 8) + (b[2] << 16) + (b[3] << 24)
        if position >= (1 << 31):
            position -= 1 << 32
        return position
