import tict834_smbus as output_stepper
from smbus2 import SMBus, i2c_msg
import time

# Open a handle to "/dev/i2c-1", representing the I2C bus.
bus = SMBus(1)
stepper = output_stepper.TicI2C(bus, 0x0E)


def SetStepperOutput(stepper, target_position):
    stepper.exit_safe_start()
    stepper.set_target_position(target_position)


SetStepperOutput(stepper, position)