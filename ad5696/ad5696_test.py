import ad5696_smbus as output_dac
from smbus2 import SMBus
import time


i2c = SMBus(1)
dac = output_dac.AD5696(i2c, 0x0C)
outputs = {
    "I0": 65535,
    "I1": 65535,
    "V0": 40000,
    "V1": 40000,
}


def SetDACOutputs(dac, outputs):
    dac.SetOutputs(dac.DAC_A, outputs["I0"])
    dac.SetOutputs(dac.DAC_B, outputs["I1"])
    dac.SetOutputs(dac.DAC_C, outputs["V0"])
    dac.SetOutputs(dac.DAC_D, outputs["V1"])


SetDACOutputs(dac, outputs)