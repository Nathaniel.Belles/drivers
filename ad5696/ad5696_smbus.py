from smbus2 import SMBus
import time


def debug_print(string, spacing=0, backend=False):
    PRINT_LEVEL = 0
    PRINT_BACKEND = False

    result = ""
    for i in range(spacing):
        result += "\t"
    result += string

    if backend == False:
        if PRINT_LEVEL >= spacing:
            print(result)
    else:
        if PRINT_BACKEND == True:
            print(result)


class AD5696:
    def __init__(self, bus, address):
        # Debug print
        debug_print("Entering __init__()", 1, True)

        # Store arguments
        self.bus = bus
        self.address = address

        # # Run setup functions
        self.__setup_variables_ad5696()
        self.__setup_variables_config()
        self.__setup_config_i2c()
        self.__setup_config_ad5696()

        # Debug print
        debug_print("Exiting __init__()", 1, True)

    def __del__(self):
        # Set all outputs to zero before closing
        self.SetOutputs(self.DAC_ALL, 0)

    def __setup_variables_ad5696(self):
        # Debug print
        debug_print("Entering __setup_variables_ad5696()", 2, True)

        # Commands
        self.CMD_NOOP = 0b0000
        self.CMD_WRITE = 0b0001
        self.CMD_UPDATE = 0b0010
        self.CMD_WRITE_AND_UPDATE = 0b0011
        self.CMD_PWDN_PWUP = 0b0100
        self.CMD_HARDWARE_MASK = 0b0101
        self.CMD_SOFTWARE_RESET = 0b0110

        # DAC channels
        self.DAC_A = 0b0001
        self.DAC_B = 0b0010
        self.DAC_C = 0b0100
        self.DAC_D = 0b1000
        self.DAC_ALL = 0b1111

        # Power operation state
        self.STATE_NORMAL = 0b00
        self.STATE_1K = 0b01
        self.STATE_100K = 0b10
        self.STATE_TRI = 0b11

        # Debug print
        debug_print("Exiting __setup_variables_ad5696()", 2, True)

    def __setup_variables_config(self):
        # Debug print
        debug_print("Entering __setup_variables_config()", 2, True)

        # Max values for each channel
        self.DAC_A_MAX = 40000
        self.DAC_B_MAX = 40000
        self.DAC_C_MAX = 65535
        self.DAC_D_MAX = 65535

        # Debug print
        debug_print("Exiting __setup_variables_config()", 2, True)

    def __setup_config_i2c(self):
        # Debug print
        debug_print("Entering __setup_config_i2c()", 2, True)

        # Not sure what needs to be here just yet...
        # Maybe attempt communication to verify it works?

        # Debug print
        debug_print("Exiting __setup_config_i2c()", 2, True)

    def __setup_config_ad5696(self):
        # Debug print
        debug_print("Entering __setup_config_ad5696()", 2, True)

        # Reset chip
        self.Reset()

        # Send configuration bytes to the ad5696
        self.SetPowerState(
            self.STATE_NORMAL, self.STATE_NORMAL, self.STATE_NORMAL, self.STATE_NORMAL
        )

        # Debug print
        debug_print("Exiting __setup_config_ad5696()", 2, True)

    def Reset(self):
        # Debug print
        debug_print("Entering Reset()", 1)

        # Attempt to write the reset function
        try:
            # Write operation
            self.Write(self.CMD_SOFTWARE_RESET, 0, 0)
        except IOError:
            # Finished writing properly
            debug_print("Reset()", 1, True)
        else:
            # Reset command was unexpectedly acknowledged
            raise Exception("Unknown error occurred writing reset command.")

        # Debug print
        debug_print("Exiting Reset()", 1)

    def Write(self, command, channels, value):
        # Debug print
        debug_print("Entering Write()", 2)

        # Determine command byte
        command_byte = (command << 4) | channels

        # Determine top and bottom bytes
        top_byte = (value & 0xFF00) >> 8
        bottom_byte = (value & 0x00FF) >> 0

        # Debug print
        debug_print("Write: " + str(bin(top_byte)) + " " + str(bin(bottom_byte)), 3)

        # Write operation
        self.bus.write_i2c_block_data(
            self.address, command_byte, [top_byte, bottom_byte]
        )

        # Debug print
        debug_print("Exiting Write()", 2)

    def SetPowerState(self, stateA, stateB, stateC, stateD):
        # Debug print
        debug_print("Entering SetPowerState()", 1)

        # Write operation
        self.Write(
            self.CMD_PWDN_PWUP,
            0,
            ((stateD << 6) | (stateC << 4) | (stateB << 2) | (stateA << 0)),
        )

        # Debug print
        debug_print("Exiting SetPowerState()", 1)

    def Read(self, channel):
        # Debug print
        debug_print("Entering Read()", 2)

        # Read data from I2C bus
        data = self.bus.read_i2c_block_data(self.address, channel, 2)

        # Convert data
        result = (data[0] << 8) | (data[1] << 0)

        # Debug print
        debug_print("Retrieved data: " + str(data) + ", decimal: " + str(result), 3)

        # Debug print
        debug_print("Exiting Read()", 2)

        # Return result
        return result

    def SetOutputs(self, channels, value):
        # Debug print
        debug_print("Entering SetOutputs()", 1)

        # Check max values from configuration
        valueToSet = value
        if DAC_A & channels:
            valueToSet = min(valueToSet, DAC_A_MAX)
        if DAC_B & channels:
            valueToSet = min(valueToSet, DAC_B_MAX)
        if DAC_C & channels:
            valueToSet = min(valueToSet, DAC_C_MAX)
        if DAC_D & channels:
            valueToSet = min(valueToSet, DAC_D_MAX)

        # Write data to I2C bus
        self.Write(self.CMD_WRITE_AND_UPDATE, channels, valueToSet)

        # Debug print
        debug_print("Exiting SetOutputs()", 1)
