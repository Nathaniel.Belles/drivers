import mcp23008_smbus as output_relays
from smbus2 import SMBus
import time


i2c = SMBus(1)
relays = output_relays.MCP23008(i2c, 0x20)
outputs = {
    "R0": 0b0,
    "R1": 0b0,
    "R2": 0b0,
    "R3": 0b0,
    "R4": 0b0,
    "R5": 0b1,
}


def SetRelayOutputs(relays, outputs):
    relays.SetOutputs(
        relays.ConvertBitsToByte(
            [
                outputs["R0"],
                outputs["R1"],
                outputs["R2"],
                outputs["R3"],
                outputs["R4"],
                outputs["R5"],
                0,
                0,
            ]
        )
    )


SetRelayOutputs(relays, outputs)