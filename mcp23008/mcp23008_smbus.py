from smbus2 import SMBus
import time


def debug_print(string, spacing=0, backend=False):
    PRINT_LEVEL = 0
    PRINT_BACKEND = False

    result = ""
    for i in range(spacing):
        result += "\t"
    result += string

    if backend == False:
        if PRINT_LEVEL >= spacing:
            print(result)
    else:
        if PRINT_BACKEND == True:
            print(result)


class MCP23008:
    def __init__(self, bus, address):
        # Debug print
        debug_print("Entering __init__()", 1, True)

        # Store arguments
        self.bus = bus
        self.address = address

        # Run setup functions
        self.__setup_variables_mcp23008()
        self.__setup_variables_config()
        self.__setup_config_i2c()
        self.__setup_config_mcp23008()

        self.SetOutputs(0)
        print(self.ReadRegister(self.REG_IODIR))
        print(self.ReadRegister(self.REG_GPIO))

        # Debug print
        debug_print("Exiting __init__()", 1, True)

    def __del__(self):
        # Set all as outputs with state low
        self.SetGPIO(self.IODIR_ALL_OUTPUTS, self.ALL_LOW)

    def __setup_variables_mcp23008(self):
        # Debug print
        debug_print("Entering __setup_variables_mcp23008()", 2, True)

        # Registers
        self.REG_IODIR = 0x00
        self.REG_IPOL = 0x01
        self.REG_GPINTEN = 0x02
        self.REG_DEFVAL = 0x03
        self.REG_INTCON = 0x04
        self.REG_IOCON = 0x05
        self.REG_GPPU = 0x06
        self.REG_INTF = 0x07
        self.REG_INTCAP = 0x08
        self.REG_GPIO = 0x09
        self.REG_OLAT = 0x0A

        # I/O Direction
        self.IODIR_OUTPUT = 0x0
        self.IODIR_INPUT = 0x1
        self.IODIR_ALL_OUTPUTS = 0x00
        self.IODIR_ALL_INPUTS = 0xFF

        # GPIO Input Polarity
        self.IPOL_NORMAL = 0x00
        self.IPOL_INVERTED = 0x01
        self.IPOL_ALL_NORMAL = 0x00
        self.IPOL_ALL_INVERTED = 0xFF

        # GPIO Interrupt-on-Change
        self.GPINTEN_DISABLED = 0x00
        self.GPINTEN_ENABLED = 0x01
        self.GPINTEN_ALL_DISABLED = 0x00
        self.GPINTEN_ALL_ENABLED = 0xFF

        # Default Compare Value Register
        self.DEFVAL_ZERO = 0x00
        self.DEFVAL_ONE = 0x01
        self.DEFVAL_ALL_ZERO = 0x00
        self.DEFVAL_ALL_ONE = 0xFF

        # Interrupt-on-Change Control
        self.INTCON_COMPARE_TO_PREVIOUS = 0x00
        self.INTCON_COMPARE_TO_DEFVAL = 0x01
        self.INTCON_ALL_COMPARE_TO_PREVIOUS = 0x00
        self.INTCON_ALL_COMPARE_TO_DEFVAL = 0xFF

        # Configuration Register
        self.IOCON_SEQOP_ENABLE = 0x00  # Enable Auto-increment Address Pointer
        self.IOCON_SEQOP_DISABLE = 0x20  # Disable Auto-increment Address Pointer
        self.IOCON_DISSLW_ENABLE = 0x00  # Enable slew rate on SDA Output
        self.IOCON_DISSLW_DISABLE = 0x10  # Disable slew rate on SDA Output
        self.IOCON_ODR_DRIVE = 0x00  # Driven output (INTPOL bit sets the polarity)
        self.IOCON_ODR_DRAIN = 0x04  # Open-drain output (overrides the INTPOL bit)
        self.IOCON_INTPOL_ACTIVE_LOW = 0x00  # Active-low interrupt output pin
        self.IOCON_INTPOL_ACTIVE_HIGH = 0x02  # Active-high interrupt output pin

        # GPIO Pull-Up Resistor
        self.GPPU_DISABLED = 0x00
        self.GPPU_ENABLED = 0x01
        self.GPPU_ALL_DISABLED = 0x00
        self.GPPU_ALL_ENABLED = 0xFF

        # I/O
        self.LOW = 0x00
        self.HIGH = 0x01
        self.ALL_LOW = 0x00
        self.ALL_HIGH = 0xFF

        # Output Latch
        self.OLAT_LOGIC_LOW = 0x00
        self.OLAT_LOGIC_HIGH = 0x01
        self.OLAT_ALL_LOGIC_LOW = 0x00
        self.OLAT_ALL_LOGIC_HIGH = 0xFF

        # Debug print
        debug_print("Exiting __setup_variables_mcp23008()", 2, True)

    def __setup_variables_config(self):
        # Debug print
        debug_print("Entering __setup_variables_config()", 2, True)

        # IODIR
        self.CONF_00 = self.IODIR_ALL_OUTPUTS

        # IPOL
        self.CONF_01 = self.IPOL_ALL_NORMAL

        # GPINTEN
        self.CONF_02 = self.GPINTEN_ALL_DISABLED

        # DEFVAL
        self.CONF_03 = self.DEFVAL_ALL_ZERO

        # INTCON
        self.CONF_04 = self.INTCON_ALL_COMPARE_TO_DEFVAL

        # IOCON
        self.CONF_05 = (
            self.IOCON_SEQOP_ENABLE
            | self.IOCON_DISSLW_ENABLE
            | self.IOCON_ODR_DRIVE
            | self.IOCON_INTPOL_ACTIVE_LOW
        )

        # GPPU
        self.CONF_06 = self.GPPU_ALL_ENABLED

        # INTF - Read-Only Register
        self.CONF_07 = 0x00

        # INTCAP - Read-Only Register
        self.CONF_08 = 0x00

        # GPIO
        self.CONF_09 = self.ALL_LOW

        # OLAT
        self.CONF_0A = self.OLAT_ALL_LOGIC_HIGH

        # Debug print
        debug_print("Exiting __setup_variables_config()", 2, True)

    def __setup_config_i2c(self):
        # Debug print
        debug_print("Entering __setup_config_i2c()", 2, True)

        # Not sure what needs to be here just yet...
        # Maybe attempt communication to verify it works?

        # Debug print
        debug_print("Exiting __setup_config_i2c()", 2, True)

    def __setup_config_mcp23008(self):
        # Debug print
        debug_print("Entering __setup_config_mcp23008()", 2, True)

        config = [
            self.CONF_00,
            self.CONF_01,
            self.CONF_02,
            self.CONF_03,
            self.CONF_04,
            self.CONF_05,
            self.CONF_06,
            self.CONF_07,
            self.CONF_08,
            self.CONF_09,
            self.CONF_0A,
        ]

        # Send all configuration bytes to the mcp23008
        self.WriteRegister(self.REG_IODIR, config)

        # Debug print
        debug_print("Exiting __setup_config_mcp23008()", 2, True)

    def ConvertBitsToByte(self, list):
        result = 0

        for i in range(len(list)):
            result += list[i] * (2 ** i)

        return result

    def ReadRegister(self, register, length=1):
        # Debug print
        debug_print("Entering ReadRegister()", 3)

        # Read data from I2C bus
        data = self.bus.read_i2c_block_data(self.address, register, length)

        # Debug print
        debug_print("Retrieved data: " + str(data), 4)

        # Debug print
        debug_print("Exiting ReadRegister()", 3)

        # Return data
        return data

    def WriteRegister(self, register, data):
        # Debug print
        debug_print("Entering ReadRegister()", 3)

        # Write data to I2C bus
        self.bus.write_i2c_block_data(self.address, register, data)

        # Debug print
        debug_print("Exiting ReadRegister()", 3)

    def SetGPIO(self, direction, state):
        # Debug print
        debug_print("Entering SetGPIO()", 2)

        # Set the GPIO direction
        self.WriteRegister(self.REG_IODIR, [direction])

        # Set the GPIO state
        self.WriteRegister(self.REG_GPIO, [state])

        # Debug print
        debug_print("Exiting SetGPIO()", 2)

    def SetOutputs(self, state):
        # Debug print
        debug_print("Entering SetOutputs()", 1)

        # Use SetGPIO() to set all the pins as outputs
        self.SetGPIO(self.IODIR_ALL_OUTPUTS, state)

        # Debug print
        debug_print("Exiting SetOutputs()", 1)

    def SetInputs(self, state):
        # Debug print
        debug_print("Entering SetInputs()", 1)

        # Use SetGPIO() to set all the pins as outputs
        self.SetGPIO(self.IODIR_ALL_OUTPUTS, state)

        # Debug print
        debug_print("Exiting SetInputs()", 1)
